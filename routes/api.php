<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', [\App\Http\Controllers\AuthController::class, 'getCurrentUser'])->name('auth.me');

    Route::get('/logout', [\App\Http\Controllers\AuthController::class, 'logout'])
        ->name('auth.logout');

    Route::get('/employees', [\App\Http\Controllers\EmployeeController::class, 'get'])
        ->name('employees.get');

    Route::get('/employees/{employee}', [\App\Http\Controllers\EmployeeController::class, 'show'])
        ->name('employees.show');

    Route::post('/employees', [\App\Http\Controllers\EmployeeController::class, 'import'])
        ->name('employees.import');

    Route::delete('/employees/{employee}', [\App\Http\Controllers\EmployeeController::class, 'destroy'])
        ->name('employees.destroy');
});

Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login'])
    ->name('auth.login');
