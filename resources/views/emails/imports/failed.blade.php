@component('mail::message')
# Ola, {{$user['name']}}

Venho por meio desse e-mail informa-lo(a) que a sua importação acabou ocorrendo um erro inesperado, tente novamente, por favor.

Atenciosamente,<br>
{{ config('app.name') }}
@endcomponent