@component('mail::message')
# Ola, {{$user['name']}}

Venho por meio desse e-mail informa-lo(a) que a sua importação no nosso sistema foi finalizada com sucesso.

Atenciosamente,<br>
{{ config('app.name') }}
@endcomponent