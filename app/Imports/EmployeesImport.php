<?php

namespace App\Imports;

use App\Mail\ImportHasFailedMail;
use App\Mail\ImportsFinishedMail;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithUpserts;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\ImportFailed;
use Maatwebsite\Excel\Concerns\Importable;

class EmployeesImport implements ToModel, WithHeadingRow, WithChunkReading, WithUpserts, ShouldQueue, WithEvents
{
    use Importable;
    protected User $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function model(array $row)
    {
        return new Employee([
            'user_id' => $this->user->id,
            'name' => $row['name'],
            'email' => $row['email'],
            'document' => $row['document'],
            'city' => $row['city'],
            'state' => $row['state'],
            'start_date' => $row['start_date'],
        ]);
    }

    public function chunkSize(): int
    {
        return 100;
    }

    public function uniqueBy()
    {
        return ['document', 'user_id'];
    }

    public function registerEvents(): array
    {
        return [
            AfterImport::class => function () {
                Mail::to($this->user->email)->send(new ImportsFinishedMail($this->user));
            },
            ImportFailed::class => function () {
                Mail::to($this->user->email)->send(new ImportHasFailedMail($this->user));
            },
        ];
    }
}
