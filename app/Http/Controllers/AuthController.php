<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginUserRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController
{
    public function login(LoginUserRequest $request): JsonResponse
    {
        $credentials = $request->validated();

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Incorrect username or password'
            ], 422);
        }

        $authUser = Auth::user();
        $token = $authUser->createToken('token')->accessToken;

        return response()->json([
            'user' => $authUser,
            'token' => $token
        ], 200);
    }
    public function logout(): JsonResponse
    {
        Auth::user()->token()->revoke();

        return response()->json([
            'message' => 'You have been successfully logged out'
        ], 200);
    }
    public function getCurrentUser(Request $request): JsonResponse
    {
        return response()->json([
            'user' => Auth::user(),
            'token' => $request->bearerToken()
        ], 200);
    }
}
