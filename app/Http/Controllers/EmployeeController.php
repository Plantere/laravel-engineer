<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImportEmployeeRequest;
use App\Imports\EmployeesImport;
use App\Models\Employee;
use App\Services\ImportServices;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

ini_set('max_execution_time', '120');
class EmployeeController extends Controller
{
    public function get(): JsonResponse
    {
        return response()->json(
            Employee::where('user_id', auth()->user()->id)->get()
        );
    }

    public function show(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json(
            $employee
        );
    }

    public function destroy(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json([
            'Success' => $employee->delete()
        ]);
    }

    public function import(ImportEmployeeRequest $request, ImportServices $ImportServices): JsonResponse
    {
        $attributes = $request->validated();
        $validation = $ImportServices->validateEmployeeFile($attributes['file']);

        if ($validation !== 'ACCEPTABLE') {
            return response()->json([
                'error' => $validation
            ], 406);
        }

        Excel::queueImport(
            new EmployeesImport(Auth::user()),
            $attributes['file']
        );

        return response()->json([
            'message' => "the file is valid and the import started successfully"
        ]);
    }
}
