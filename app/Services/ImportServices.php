<?php

namespace App\Services;

class ImportServices
{
    public function validateEmployeeFile(string $path): string
    {
        $responseDelimiter = $this->detectDelimiterAndGetHeader($path);

        $headerCsv = $responseDelimiter['header'];
        $delimiterCsv = $responseDelimiter['delimiter'];

        $responseValidateHeader = $this->validateHeader($headerCsv, ['name', 'email', 'document', 'city', 'state', 'start_date']);
        if (!$responseValidateHeader) {
            return "NOT ACCEPTABLE: Invalid Header";
        }

        return $this->validateColumnsWithCustomRegex($path, $delimiterCsv, $headerCsv);
    }

    private function detectDelimiterAndGetHeader(string $path): array
    {
        $delimiters = [';' => 0, ',' => 0, "\t" => 0, '|' => 0];

        $handle = fopen($path, "r");
        $header = fgets($handle);
        fclose($handle);
        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($header, $delimiter));
        }
        $delimiter = array_search(max($delimiters), $delimiters);
        $header = preg_replace('/\s+/', "", $header);
        return [
            'delimiter' => $delimiter,
            'header' => explode($delimiter, $header)
        ];
    }

    private function validateHeader(array $header, array $expected): bool
    {
        $diff = array_diff($expected, $header);
        if (count($diff)) {
            return false;
        }

        return true;
    }

    private function validateColumnsWithCustomRegex(string $path, string $delimiter, array $header)
    {
        $validations = [
            'start_date' => [
                'regex' => '/^((((19|[2-9]\d)\d{2})\-(0[13578]|1[02])\-(0[1-9]|[12]\d|3[01]))|(((19|[2-9]\d)\d{2})\-(0[13456789]|1[012])\-(0[1-9]|[12]\d|30))|(((19|[2-9]\d)\d{2})\-02\-(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))\-02\-29))$/',
                'message' => 'NOT ACCEPTABLE: Invalid Date'
            ],
            'email' => [
                'regex' => '/^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}$/',
                'message' => 'NOT ACCEPTABLE: Invalid Email'
            ],
            'document' => [
                'regex' => '/^\d{11}$/',
                'message' => 'NOT ACCEPTABLE: Invalid Document'
            ],
            'city' => [
                'regex' => "/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ]+( [A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ]+)*$/",
                'message' => 'NOT ACCEPTABLE: Invalid City'
            ],
            'state' => [
                'regex' => "/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ]+( [A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ]+)*$/",
                'message' => 'NOT ACCEPTABLE: Invalid State'
            ],
            'name' => [
                'regex' => "/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ]{3,}+( [A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ]{2,}+)*$/",
                'message' => 'NOT ACCEPTABLE: Invalid Name'
            ],
        ];


        $contentFile = file($path);
        array_shift($contentFile);
        $keys = array_flip($header);

        $extractedCsv = [];
        foreach ($contentFile as $row) {
            $extractedCsv['start_date'][] = preg_replace('/\s+/', "", explode($delimiter, $row)[$keys['start_date']]);
            $extractedCsv['email'][] = preg_replace('/\s+/', "", explode($delimiter, $row)[$keys['email']]);
            $extractedCsv['document'][] = preg_replace('/\s+/', "", explode($delimiter, $row)[$keys['document']]);
            $extractedCsv['city'][] = preg_replace('/\s+/', "", explode($delimiter, $row)[$keys['city']]);
            $extractedCsv['state'][] = preg_replace('/\s+/', "", explode($delimiter, $row)[$keys['state']]);
            $extractedCsv['name'][] = preg_replace('/\s+/', "", explode($delimiter, $row)[$keys['name']]);
        }
        $countableEmployees = count($contentFile);

        foreach ($validations as $key => $validation) {
            $countableValids = count(preg_grep($validation['regex'], $extractedCsv[$key]));
            if ($countableValids !== $countableEmployees) {
                return $validation['message'];
            }
        }
        return 'ACCEPTABLE';
    }
}
