<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Passport;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('passport:install'); // this is because we are usign a inmemory database for testing
    }

    public function test_it_user_cannot_login_with_password_wrong()
    {
        $user = User::factory()->create([
            'email' => 'test@convenia-test-user.com',
            'password' => 'password123'
        ]);

        $credentials = [
            'email' => $user->email,
            'password' => 'some_password_wrong'
        ];

        $this->post(route('auth.login'), $credentials)
            ->assertStatus(422)
            ->assertJson(['message' => 'Incorrect username or password']);
    }


    public function test_it_user_cannot_login_with_email_wrong()
    {
        User::factory()->create([
            'email' => 'test@convenia-test-user.com',
            'password' => 'password123'
        ]);

        $credentials = [
            'email' => 'some_wrong_email@convenia_test_user.com',
            'password' => 'password123'
        ];

        $this->post(route('auth.login'), $credentials)
            ->assertStatus(422)
            ->assertJson(['message' => 'Incorrect username or password']);
    }

    public function test_it_user_logged_successfully()
    {
        $user = User::factory()->create(
            [
                'email' => 'anastasia@convenia_test_user.com',
                'password' => Hash::make('password')
            ]
        );

        $credentials = [
            'email' => $user->email,
            'password' => 'password'
        ];
        $this->post(route('auth.login'), $credentials)
            ->assertStatus(200)
            ->assertJsonStructure(['user' => ['id', 'name', 'email', 'email_verified_at', 'created_at', 'updated_at'], 'token']);
    }

    public function test_it_user_can_log_out_successfully()
    {
        $user = User::factory()->create(
            [
                'email' => 'anastasia@convenia_test_user.com',
                'password' => Hash::make('password')
            ]
        );

        $credentials = [
            'email' => $user->email,
            'password' => 'password'
        ];

        $token = $this->post(route('auth.login'), $credentials)->decodeResponseJson()['token'];

        $this->get(route('auth.logout'), ['Authorization' => 'Bearer ' . $token])
            ->assertStatus(200)
            ->assertJson([
                'message' => 'You have been successfully logged out'
            ]);
    }
    public function test_it_getting_data_from_current_user_logged()
    {
        $user = User::factory()->create(
            [
                'email' => 'anastasia@convenia_test_user.com',
                'password' => Hash::make('password')
            ]
        );

        $credentials = [
            'email' => $user->email,
            'password' => 'password'
        ];

        $token = $this->post(route('auth.login'), $credentials)->decodeResponseJson()['token'];

        $this->get(route('auth.me'), ['Authorization' => 'Bearer ' . $token])
            ->assertStatus(200)
            ->assertJson([
                'user' => [
                    "id" => $user->id,
                    "name" => $user->name,
                    "email" => $user->email,
                    "email_verified_at" => $user->email_verified_at->jsonSerialize(),
                    "created_at" => $user->created_at->jsonSerialize(),
                    "updated_at" => $user->updated_at->jsonSerialize()
                ],
                'token' => $token
            ]);
    }
}
