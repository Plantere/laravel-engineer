<?php

namespace Tests\Feature;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Laravel\Passport\Passport;
use Maatwebsite\Excel\Facades\Excel;
use Tests\TestCase;

class EmployeeControllerTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = Passport::actingAs(
            User::factory()->create(),
            ['create-servers']
        );
    }

    public function test_it_gets_all_employees_with_a_auth_user()
    {
        $employees = Employee::factory(3)->create([
            'user_id' => $this->user->id
        ]);

        $this->get(route('employees.get'))
            ->assertOk()
            ->assertJson(
                $employees->map(function ($employee) {
                    return [
                        'id' => $employee->id,
                        'user_id' => $employee->user_id,
                        'name' => $employee->name,
                        'email' => $employee->email,
                        'document' => $employee->document,
                        'city' => $employee->city,
                        'state' => $employee->state,
                        'start_date' => $employee->start_date,
                    ];
                })->toArray()
            );
    }

    public function test_it_cannot_gets_employees_from_another_user()
    {
        $user = User::factory()->create();

        Employee::factory(3)->create([
            'user_id' => $user->id
        ]);

        $this->get(route('employees.get'))
            ->assertOk()
            ->assertExactJson([]);
    }

    public function test_it_shows_a_employee_with_a_auth_user()
    {
        $employee = Employee::factory()->create([
            'user_id' => $this->user->id
        ]);

        $this->get(route('employees.show', [$employee->id]),)
            ->assertOk()
            ->assertJson([
                'id' => $employee->id,
                'user_id' => $employee->user_id,
                'name' => $employee->name,
                'email' => $employee->email,
                'document' => $employee->document,
                'city' => $employee->city,
                'state' => $employee->state,
                'start_date' => $employee->start_date,
            ]);
    }

    public function test_it_cannot_shows_a_employee_from_another_user()
    {
        $user = User::factory()->create();

        $employee = Employee::factory()->create([
            'user_id' => $user->id
        ]);

        $this->get(route('employees.show', [$employee->id]))
            ->assertForbidden()
            ->assertSee('This action is unauthorized.');
    }

    public function test_it_deletes_a_employee_with_a_auth_user()
    {
        $employee = Employee::factory()->create([
            'user_id' => $this->user->id
        ]);

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id
        ]);

        $this->delete(route('employees.destroy', [$employee->id]),)
            ->assertOk()
            ->assertExactJson([
                'Success' => true
            ]);

        $this->assertDatabaseMissing('employees', [
            'id' => $employee->id
        ]);
    }

    public function test_it_cannot_deletes_a_employee_from_another_user()
    {
        $user = User::factory()->create();

        $employee = Employee::factory()->create([
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id,
            'user_id' => $user->id
        ]);

        $this->delete(route('employees.destroy', [$employee->id]),)
            ->assertForbidden()
            ->assertSee('This action is unauthorized.');

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id,
            'user_id' => $user->id
        ]);
    }

    public function test_it_import_successfully_new_employees()
    {

        $content = file_get_contents(base_path() . "/tests/Feature/mocks/test_import_employee_successfully.csv");
        $file = UploadedFile::fake()->createWithContent('test1.csv', $content);
        $attributes = [
            'file' => $file
        ];

        Excel::fake();

        $this->post(route('employees.import'), $attributes)
            ->assertStatus(200)
            ->assertJson([
                'message' => "the file is valid and the import started successfully"
            ]);

        Excel::assertQueued($file->getFilename());
        Excel::assertImported($file->getFilename());
    }

    public function test_it_cannot_import_any_employee_with_columns_wrong()
    {
        $tests = [
            'header' => ['file' => 'test_import_employee_with_header_wrong.csv', 'message' => 'NOT ACCEPTABLE: Invalid Header'],
            'document' => ['file' => 'test_import_employee_with_document_wrong.csv', 'message' => 'NOT ACCEPTABLE: Invalid Document'],
            'city' => ['file' => 'test_import_employee_with_city_wrong.csv', 'message' => 'NOT ACCEPTABLE: Invalid City'],
            'state' => ['file' => 'test_import_employee_with_state_wrong.csv', 'message' => 'NOT ACCEPTABLE: Invalid State'],
            'name' => ['file' => 'test_import_employee_with_name_wrong.csv', 'message' => 'NOT ACCEPTABLE: Invalid Name'],
            'start_date' => ['file' => 'test_import_employee_with_start_date_wrong.csv', 'message' => 'NOT ACCEPTABLE: Invalid Date'],
        ];

        foreach ($tests as $test) {
            $content = file_get_contents(base_path() . "/tests/Feature/mocks/{$test['file']}");
            $file = UploadedFile::fake()->createWithContent('test2.csv', $content);
            $this->post(route('employees.import'), ['file' => $file])
                ->assertStatus(406)
                ->assertJson([
                    'error' => $test['message']
                ]);
        }
    }
}
